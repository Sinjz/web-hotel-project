@extends('layouts.app')

@section('title')
    <title>Manage Room</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Room</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Room</a></li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-primary">
                    {{-- <div class="card-header">
                      <h3 class="card-title">Quick Example</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ !empty($room) ? route('room.update', ['room' => $room->id]) : route('room.store') }}">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                            <label for="category">Kategori Kamar</label>
                            <select class="form-control" id="category" name="room_category_id">
                                @foreach ($category as $item)
                                <option value="{{ $item->id }}" {{ old('room_category_id', @$item->id == @$room->room_category_id ? 'selected' : '') }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                          <label for="room_number">Nomor Kamar</label>
                          <input type="text" class="form-control" name="room_number" id="room_number" placeholder="Nomor Kamar" value="{{ old('room_number', @$room->room_number) }}">
                        </div>
                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" id="status" name="status">
                                <option value="Available" {{ old('status', @$room->status == 'Available' ? 'selected' : '') }}>Available</option>
                                <option value="Not Available" {{ old('status', @$room->status == 'Not Available' ? 'selected' : '') }}>Not Available</option>
                            </select>
                        </div>
                      </div>

                      {{-- <div class="form-group">
                        <label for="jabatan">Select</label>
                        <select class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div> --}}
                      <!-- /.card-body -->

                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection
