<?php

use App\Http\Controllers\CategoryRoomController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\FacilityController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\RoomImageController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
// Route::get('/test', [CustomerController::class, 'test1']);
// Route::post('/register-test', [CustomerController::class, 'testing'])->name('test');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('category', CategoryRoomController::class);
    Route::group(['prefix' => 'category'], function() {
        Route::post('/update/{category}', [CategoryRoomController::class, 'update'])->name('category.update');
        Route::get('/destroy/{category}', [CategoryRoomController::class, 'destroy'])->name('category.destroy');
    });

    Route::resource('user', UserController::class);
    Route::group(['prefix' => 'user'], function() {
        Route::post('/update/{user}', [UserController::class, 'update'])->name('user.update');
        Route::get('/destroy/{user}', [UserController::class, 'destroy'])->name('user.destroy');
    });

    Route::resource('room', RoomController::class);
    Route::group(['prefix' => 'room'], function() {
        Route::post('/update/{room}', [RoomController::class, 'update'])->name('room.update');
        Route::get('/destroy/{room}', [RoomController::class, 'destroy'])->name('room.destroy');
    });

    Route::resource('customer', CustomerController::class);
    Route::group(['prefix' => 'customer'], function() {
        Route::post('/update/{customer}', [CustomerController::class, 'update'])->name('customer.update');
        Route::get('/destroy/{customer}', [CustomerController::class, 'destroy'])->name('customer.destroy');
    });

    Route::resource('facility', FacilityController::class);
    Route::group(['prefix' => 'facility'], function() {
        Route::post('/update/{facility}', [FacilityController::class, 'update'])->name('facility.update');
        Route::get('/destroy/{facility}', [FacilityController::class, 'destroy'])->name('facility.destroy');
    });

    Route::resource('order', OrderController::class);
    Route::group(['prefix' => 'order'], function() {
        Route::post('/update/{order}', [OrderController::class, 'update'])->name('order.update');
        Route::get('/invoice/{id}/print', [OrderController::class, 'print'])->name('order.print');
    });
});


