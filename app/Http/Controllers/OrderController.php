<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Room;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = Order::with(['orderDetail.room','customer'])
        ->get();

        return view('admin.order.index', compact('order'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::with(['orderDetail.room','orderDetail.roomCategory','customer'])
        ->where('id', $id)
        ->first();


        return view('admin.order.show', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findorFail($id);
        $room = Room::where('status', 'available')
        ->where('room_category_id', $order->orderDetail->roomCategory->id)
        ->get();

        $data = array(
            'order' => $order,
            'room' => $room
        );

        return view('admin.order.manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'room_id' => ['required']
            ]);

            $order = Order::findorFail($id);
            $data = $request->except('_data');

            $order->status = 'Success';
            $order->orderDetail->room_id = $data['room_id'];
            $order->orderDetail->room->status = 'not available';

            $order->orderDetail->room->update();
            $order->orderDetail->update();
            $order->update();

            return redirect()->route('order.index')->with('success', 'Success Update Order');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
