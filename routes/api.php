<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\EmployeeController;
use App\Http\Controllers\API\FacilityController;
use App\Http\Controllers\API\OrderController;
use App\Http\Controllers\API\RoomController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

// get data
Route::get('/room', [RoomController::class, 'indexRoom']);
Route::get('/room-category', [RoomController::class, 'indexCategory']);
Route::get('/room-image', [RoomController::class, 'indexImage']);
Route::get('/facility', [FacilityController::class, 'indexFacility']);
Route::get('/employee', [EmployeeController::class, 'indexEmployee']);

// customer action
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::middleware('auth:sanctum')->group(function() {
    Route::post('/check-out', [OrderController::class, 'checkOut']);
    Route::get('/get-customer', [AuthController::class, 'getCustomer']);
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::get('/customer-order', [AuthController::class, 'getCustomerOrder']);
});
