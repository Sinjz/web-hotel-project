<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Facility;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    public function indexFacility()
    {
        $facility = Facility::with(['roomCategory'])
        ->get();

        if($facility)
            return ResponseFormatter::success(
                $facility,
                'Facility data retrieved successfully'
            );
        else
            return ResponseFormatter::error(
                null,
                'Room data not exist',
                404,
            );
    }
}
