<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisitorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'visitor',
                'email' => 'Visitor@mail.com',
                'phone_number' => '123456789',
                'password' => bcrypt('visitor123'),
            ],
        ];

        DB::table('customer')->insert($data);
    }
}
