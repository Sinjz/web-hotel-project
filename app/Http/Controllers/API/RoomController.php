<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\CategoryRoom;
use App\Models\Room;
use App\Models\RoomImage;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    public function indexRoom()
    {
        $room = Room::with(['roomCategory','roomImage'])
        ->get();

        if($room)
            return ResponseFormatter::success(
                $room,
                'Room data retrieved successfully'
            );
        else
            return ResponseFormatter::error(
                null,
                'Room data not exist',
                404,
            );

    }

    public function indexCategory()
    {
        $category = CategoryRoom::with(['room','facility'])
        ->get();

        if($category)
            return ResponseFormatter::success(
                $category,
                'Room Category data retrieved successfully'
            );
        else
            return ResponseFormatter::error(
                null,
                'Room data not exist',
                404,
            );

    }

    public function indexImage()
    {
        $images = RoomImage::with(['room'])
        ->get();

        if($images)
            return ResponseFormatter::success(
                $images,
                'Room Image data retrieved successfully'
            );
        else
            return ResponseFormatter::error(
                null,
                'Room Image data not exist',
                404,
            );
    }
}
