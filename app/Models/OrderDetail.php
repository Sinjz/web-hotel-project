<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'order_detail';

    protected $fillable = [
        'order_id','room_id','duration_stay','room_category_id'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id','id');
    }

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function roomCategory()
    {
        return $this->belongsTo(CategoryRoom::class, 'room_category_id', 'id');
    }
}
