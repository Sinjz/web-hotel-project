<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CustomerController extends Controller
{
    public function test1()
    {
        return view('test');
    }

    public function testing(Request $request)
    {
        $data = $request->all();

        $url = 'https://web-hotel-rpl.my.id/api/register';

        $responses = Http::post($url, $data);

        return $responses;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::all();

        return view('admin.customer.index', compact('customer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.customer.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'email' => ['required','email'],
                'phone_number' => ['required','numeric'],
                'position' => ['required'],
                'address' => ['required'],
                // 'password' => ['required'],
            ]);

            $data = $request->except('_token');
            $data['password'] = bcrypt($data['password']);
            // dd($data);

            Customer::create($data);

            return redirect()->route('customer.index')->with('success', 'Success Create Pelanggan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customer = Customer::findorFail($id);

        return view('admin.customer.manage', compact('customer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'email' => ['required','email'],
                'phone_number' => ['required','numeric'],
                // 'password' => ['required'],
            ]);

            $user = Customer::findorFail($id);

            $data = $request->except('_token');
            $data['password'] = bcrypt($data['password']);

            $user->update($data);

            return redirect()->route('customer.index')->with('success', 'Success Edit Pelanggan');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Customer::findorFail($id);

        $user->delete();

        return redirect()->route('customer.index')->with('success', 'Success Delete Customer');
    }
}
