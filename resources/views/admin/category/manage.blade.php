@extends('layouts.app')

@section('title')
    <title>Manage Category</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Category</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Category</a></li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-primary">
                    {{-- <div class="card-header">
                      <h3 class="card-title">Quick Example</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ !empty($category) ? route('category.update', ['category' => $category->id]) : route('category.store') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                          <label for="name">Category Name</label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="Category Name" value="{{ old('name', @$category->name) }}">
                        </div>
                        <div class="form-group">
                          <label for="price">Price</label>
                          <input  class="form-control" name="price" id="price" type="number" placeholder="Price" value="{{ old('price', @$category->price) }}">
                        </div>
                        <div class="form-group">
                            <label for="wide">Wide</label>
                            <input class="form-control" name="wide" id="wide" placeholder="Wide Room" value="{{ old('wide', @$category->wide) }}">
                        </div>
                        <div class="form-group">
                            <label for="capacity">Capacity</label>
                            <input class="form-control" name="capacity" id="capacity" type="number" placeholder="Capacity Room" value="{{ old('capacity', @$category->capacity) }}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputFile">Upload Image</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="url_image" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                          </div>
                        {{-- <div class="form-group">
                          <label for="exampleInputFile">File input</label>
                          <div class="input-group">
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="exampleInputFile">
                              <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                            <div class="input-group-append">
                              <span class="input-group-text">Upload</span>
                            </div>
                          </div>
                        </div> --}}
                      </div>
                      <!-- /.card-body -->

                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection
