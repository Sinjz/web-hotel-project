<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'admin',
                'email' => 'admin@mail.com',
                'phone_number' => '1111111',
                'address' => 'admin address',
                'position' => 'Admin',
                'password' => bcrypt('admin123'),
            ],
        ];

        DB::table('users')->insert($data);
    }
}
