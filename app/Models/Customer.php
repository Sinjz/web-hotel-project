<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Authenticatable
{
    use HasApiTokens, SoftDeletes;

    protected $table = 'customer';

    protected $fillable = [
        'name','email','phone_number','password'
    ];

    protected $hidden = [
        'password',
    ];

    public function order()
    {
        return $this->hasMany(Order::class, 'customer_id', 'id');
    }
}
