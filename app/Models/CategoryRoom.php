<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CategoryRoom extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'room_category';

    protected $fillable = [
        'name','price','wide','capacity','url_image'
    ];

    public function room()
    {
        return $this->hasMany(Room::class, 'room_category_id', 'id');
    }

    public function facility()
    {
        return $this->hasMany(Facility::class, 'room_category_id', 'id');
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class, 'room_category_id', 'id');
    }

}
