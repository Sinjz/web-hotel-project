@extends('layouts.app')

@section('title')
    <title>Manage Order</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Order</a></li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-primary">
                    {{-- <div class="card-header">
                      <h3 class="card-title">Quick Example</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ route('order.update', ['order' => $order->id])}}">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                            <label for="category">Nomor Kamar yang Akan di Tempati Pelanggan</label>
                            <select class="form-control" id="room" name="room_id">
                                @foreach ($room as $item)
                                <option value="{{ $item->id }}" {{ old('room_id', @$item->id == @$order->room_id ? 'selected' : '') }}>{{ $item->room_number }}</option>
                                @endforeach
                            </select>
                        </div>
                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection
