<?php

namespace App\Http\Controllers;

use App\Models\CategoryRoom;
use App\Models\Room;
use Exception;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $room = Room::all();

        return view('admin.room.index', compact('room'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = CategoryRoom::all();

        return view('admin.room.manage', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'room_category_id' => ['required'],
                'room_number' => ['required','unique:room,room_number'],
                'status' => ['required'],
            ]);

            $data = $request->except('_data');

            Room::create($data);

            return redirect()->route('room.index')->with('success', 'Success Create Kamar');
        } catch (Exception $error) {
            return redirect()->back()->with('error', $error->validator->errors());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $room = Room::findorFail($id);
        $category = CategoryRoom::all();

        $data = array(
            'room' => $room,
            'category' => $category,
        );

        return view('admin.room.manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'room_category_id' => ['required'],
                'room_number' => ['required','unique:room,room_number,'.$id],
                'status' => ['required'],
            ]);

            $room = Room::findorFail($id);
            $data = $request->except('_data');

            $room->update($data);

            return redirect()->route('room.index')->with('success', 'Success Update Kamar');
        } catch (Exception $error) {
            return redirect()->back()->with('error', $error->validator->errors());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $room = Room::findorFail($id);
        $room->status = 'not available';
        $room->update();

        $room->delete();

        return redirect()->route('room.index')->with('success', 'Success Delete Kamar');
    }
}
