@extends('layouts.app')

@section('title')
    <title>Facility</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Fasilitas</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Fasilitas</a></li>
              <li class="breadcrumb-item active">Index</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title p-3">Table Fasilitas</h3>
                            <div class="card-tools p-3">
                                <a href="{{ route('facility.create') }}" class="d-relative btn ms-auto btn btn-primary">Create Data</a>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Gambar Fasilitas</th>
                                <th>Fasilitas</th>
                                <th>Deskripsi</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($facility as $item)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><img src="{{ asset('storage/facility/image/'.$item->url_image) }}" alt="..." style="width: 50%" class="img-fluid img-thumbnail"></td>
                                <td>{{ $item->roomCategory->name }}
                                </td>
                                <td>{{ $item->description }}</td>
                                <td>
                                    <a href="{{ route('facility.edit', ['facility' => $item->id]) }}" class="btn btn-primary "><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{ route('facility.destroy', ['facility' => $item->id]) }}" onclick="return confirmDelete()" class="btn btn-danger "><i class="fa fa-trash"></i> Delete</a>
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
      <script>
        function confirmDelete() {
            return confirm('Apakah Anda yakin ingin menghapus data ini?');
        }
    </script>
@endsection
