<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\CategoryRoom;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Room;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function checkOut(Request $request)
    {
        $this->validate($request,[
            'total_price' => 'required','integer',
            'check_in' => 'required|date',
            'check_out' => 'required|date',
            'duration_stay' => 'required',
            'jenisKamar' => 'required',
        ]);

        $timestamp = Date('YmdHis');
        $mytime = Carbon::now();
        $data = $request->all();

        $customer = auth()->user();
        $room_category = CategoryRoom::where('name', $data['jenisKamar'])
        ->first();

        $order = Order::create([
            'customer_id' => $customer->id,
            'invoice_number' => $timestamp,
            'date' => $mytime,
            'total_price' => $data['total_price'],
            'payment_type' => 'cash',
            'status' => 'Confirmation Room',
            'check_in' => $data['check_in'],
            'check_out' => $data['check_out'],
            'payment_url' => '-',
        ]);

        $order_detail = OrderDetail::create([
            'order_id' => $order->id,
            'room_category_id' => $room_category->id,
            'duration_stay' => $data['duration_stay'],
        ]);

        $data = array(
            'customer' => $customer,
            'order' => $order,
            'order_detail' => $order_detail,
        );

        return ResponseFormatter::success(
            $data,
            'Transaction success'
        );
    }
}
