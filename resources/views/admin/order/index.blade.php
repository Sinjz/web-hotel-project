@extends('layouts.app')

@section('title')
    <title>Order</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Order</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Order</a></li>
              <li class="breadcrumb-item active">Index</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <div class="card">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title p-3">Table Order</h3>
                            <div class="card-tools p-3">
                                {{-- <a href="{{ route('room.create') }}" class="d-relative btn ms-auto btn btn-primary">Create Data</a> --}}
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nomor Invoice</th>
                                <th>Kategori Kamar</th>
                                <th>Atas Nama</th>
                                <th>Check In</th>
                                <th>Check Out</th>
                                <th>Total Harga</th>
                                <th>Tipe Pembayaran</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($order as $item)
                                <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->invoice_number }}</td>
                                <td>{{ $item->orderDetail->roomCategory->name }}</td>
                                <td>{{ $item->customer->name }}</td>
                                <td>{{ $item->getCheckIn() }}</td>
                                <td>{{ $item->getCheckOut() }}</td>
                                <td>@currency($item->total_price)</td>
                                <td>{{ $item->payment_type }}</td>
                                <td>
                                    @if ($item->status == 'Success')
                                    <span class="badge bg-success">{{ $item->status }}</span>
                                    @elseif ($item->status == 'Confirmation Room')
                                    <span class="badge bg-primary">{{ $item->status }}</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('order.edit', ['order' => $item->id]) }}" class="btn btn-primary "><i class="fa fa-edit"></i> Edit</a>
                                    <a href="{{ route('order.show', ['order' => $item->id]) }}" class="btn btn-secondary "><i class="fa fa-sticky-note"></i> View</a>
                                </td>
                                </tr>
                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
              </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection
