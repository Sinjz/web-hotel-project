<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::all();

        return view('admin.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'email' => ['required','email'],
                'phone_number' => ['required','numeric'],
                'position' => ['required'],
                'address' => ['required'],
                // 'password' => ['required'],
            ]);

            $data = $request->except('_token');
            $data['password'] = bcrypt($data['password']);
            // dd($data);

            User::create($data);

            return redirect()->route('user.index')->with('success', 'Success Create Pegawai');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findorFail($id);

        return view('admin.user.manage', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'email' => ['required','email'],
                'phone_number' => ['required','numeric'],
                'position' => ['required'],
                // 'password' => ['required'],
                'address' => ['required'],
            ]);

            $user = User::findorFail($id);

            $data = $request->except('_token');
            $data['password'] = bcrypt($data['password']);

            $user->update($data);

            return redirect()->route('user.index')->with('success', 'Success Edit Pegawai');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findorFail($id);

        $user->delete();

        return redirect()->route('user.index')->with('success', 'Success Delete Pegawai');
    }
}
