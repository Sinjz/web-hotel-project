<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'room';

    protected $fillable = [
        'room_category_id','room_number','status'
    ];

    public function roomCategory()
    {
        return $this->belongsTo(CategoryRoom::class, 'room_category_id', 'id');
    }

    public function orderDetail()
    {
        return $this->hasOne(OrderDetail::class, 'room_id', 'id');
    }


}
