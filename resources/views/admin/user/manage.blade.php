@extends('layouts.app')

@section('title')
    <title>Manage User</title>
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manage Pegawai</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Pegawai</a></li>
              <li class="breadcrumb-item active">Manage</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
                <div class="card card-primary">
                    {{-- <div class="card-header">
                      <h3 class="card-title">Quick Example</h3>
                    </div> --}}
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form method="POST" action="{{ !empty($user) ? route('user.update', ['user' => $user->id]) : route('user.store') }}">
                        {{ csrf_field() }}
                      <div class="card-body">
                        <div class="form-group">
                          <label for="name">Nama</label>
                          <input type="text" class="form-control" name="name" id="name" placeholder="Nama Pegawai" value="{{ old('name', @$user->name) }}">
                        </div>
                        <div class="form-group">
                          <label for="email">Email</label>
                          <input type="email" class="form-control" name="email" id="email" placeholder="email" value="{{ old('email', @$user->email) }}">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" id="password">
                        </div>
                        <div class="form-group">
                            <label for="phone_number">No Handphone</label>
                            <input type="number" class="form-control" name="phone_number" id="phone_number" placeholder="No Handphone" value="{{ old('phone_number', @$user->phone_number) }}">
                        </div>
                        <div class="form-group">
                            <label for="capacity">Alamat</label>
                            <textarea name="address" id="" cols="30" rows="10" class="form-control">{{ old('address',@$user->address) }}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="position">Jabatan</label>
                            <input type="text" class="form-control" name="position" id="position" placeholder="Position" value="{{ old('position', @$user->position) }}">
                        </div>
                      </div>
                      {{-- <div class="form-group">
                        <label for="jabatan">Select</label>
                        <select class="form-control">
                          <option>option 1</option>
                          <option>option 2</option>
                          <option>option 3</option>
                          <option>option 4</option>
                          <option>option 5</option>
                        </select>
                      </div> --}}
                      <!-- /.card-body -->

                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </div>
                    </form>
                  </div>
            </div>
            <!-- /.row -->
          </div>
    </section>
    <!-- /.content -->
  </div>
@endsection

@section('script')
<script>
    $(function () {
      $("#example1").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
      });
    });
  </script>
@endsection
