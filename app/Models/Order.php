<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'order';

    protected $fillable = [
        'customer_id','invoice_number','date','total_price','payment_type','status','check_in',
        'check_out','payment_url',
    ];

    public function orderDetail()
    {
        return $this->hasOne(OrderDetail::class, 'order_id','id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function getCheckIn()
    {
        return Carbon::parse($this->attributes['check_in'])->translatedFormat('l, d F Y');
    }

    public function getCheckOut()
    {
        return Carbon::parse($this->attributes['check_out'])->translatedFormat('l, d F Y');
    }

    public function getDate()
    {
        return Carbon::parse($this->attributes['date'])->translatedFormat('l, d F Y');
    }
}
