<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoryRoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category_room = [
            [
                'name' => 'Deluxe Double Bed',
                'price' => '2500000',
                'wide' => '16 x 16',
                'capacity' => '6',
                'url_image' => '1692013593_download.jpeg'
            ],
            [
                'name' => 'Deluxe Single Bed',
                'price' => '1000000',
                'wide' => '12 x 12',
                'capacity' => '2',
                'url_image' => '1692013593_download.jpeg'
            ],
        ];

        $room = [
            [
                'room_category_id' => '1',
                'room_number' => '101',
                'status' => 'available',
            ],
            [
                'room_category_id' => '1',
                'room_number' => '102',
                'status' => 'available',
            ],
            [
                'room_category_id' => '1',
                'room_number' => '103',
                'status' => 'available',
            ],
            [
                'room_category_id' => '1',
                'room_number' => '104',
                'status' => 'available',
            ],
            [
                'room_category_id' => '1',
                'room_number' => '105',
                'status' => 'available',
            ],
            [
                'room_category_id' => '1',
                'room_number' => '106',
                'status' => 'available',
            ],
        ];

        DB::table('room_category')->insert($category_room);
        DB::table('room')->insert($room);
    }
}
