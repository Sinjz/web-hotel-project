<?php

namespace App\Http\Controllers;

use App\Models\CategoryRoom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoryRoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = CategoryRoom::all();

        return view('admin.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'price' => ['required','numeric'],
                'wide' => ['required'],
                'capacity' => ['required','numeric'],
                'url_image' => ['required','file'],
            ]);

            $data = $request->except('_token');
            $image = $request->file('url_image');

            if ($request->hasFile('url_image')) {
                $file_name = time(). "_" . $image->getClientOriginalName();
                $image->storeAs('public/room/image', $file_name);

                $data['url_image'] = $file_name;
                CategoryRoom::create($data);
            }


            return redirect()->route('category.index')->with('success', 'Success Create Category');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = CategoryRoom::findorFail($id);

        return view('admin.category.manage', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'name' => ['required'],
                'price' => ['required','numeric'],
                'wide' => ['required'],
                'capacity' => ['required','numeric'],
            ]);

            $data = $request->except('_token');

            $category = CategoryRoom::findorFail($id);
            $image = $request->file('url_image');

            if (!empty($image)) {
                $old_path = 'public/room/image'. $category->url_image;
                Storage::delete($old_path);
                $file_name = time(). "_". $image->getClientOriginalName();
                $image->storeAs('public/room/image', $file_name);

                $data['url_image'] = $file_name;
            }


            $category->update($data);

            return redirect()->route('category.index')->with('success', 'Success Update Category');

        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = CategoryRoom::findorFail($id);

        $old_path = 'public/room/image' . $category->url_image;
        Storage::delete($old_path);

        $category->delete();

        return redirect()->route('category.index')->with('success', 'Success Delete Category');
    }
}
