<?php

namespace App\Http\Controllers;

use App\Models\CategoryRoom;
use App\Models\Facility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FacilityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $facility = Facility::all();

        return view('admin.facility.index', compact('facility'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = CategoryRoom::all();

        return view('admin.facility.manage', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validate($request,[
                'room_category_id' => ['required'],
                'name' => ['required'],
                'description' => ['required'],
                'url_image' => ['required','file'],
            ]);

            $data = $request->except('_data');
            $image = $request->file('url_image');

            if ($request->hasFile('url_image')) {
                $file_name = time(). "_" . $image->getClientOriginalName();
                $image->storeAs('public/facility/image', $file_name);

                $data['url_image'] = $file_name;
                Facility::create($data);
            }


            return redirect()->route('facility.index')->with('success', 'Success Create Fasilitas');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = Facility::findorFail($id);
        $category = CategoryRoom::all();

        $data = array(
            'facility' => $facility,
            'category' => $category
        );

        return view('admin.facility.manage', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $this->validate($request,[
                'room_category_id' => ['required'],
                'name' => ['required'],
                'description' => ['required'],
                // 'url_image' => ['required','file'],
            ]);

            $facility = Facility::findorFail($id);
            $data = $request->except('_data');
            $image = $request->file('url_image');

            if (!empty($image)) {
                $old_path = 'public/facility/image'. $facility->url_image;
                Storage::delete($old_path);
                $file_name = time(). "_". $image->getClientOriginalName();
                $image->storeAs('public/facility/image', $file_name);

                $data['url_image'] = $file_name;
            }

            $facility->update($data);

            return redirect()->route('facility.index')->with('success', 'Success Update Fasilitas');
        } catch (\Throwable $th) {
            return redirect()->back()->with('error', 'There something wrong with your input');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility = Facility::findorFail($id);

        $old_path = 'public/facility/image' . $facility->url_image;
        Storage::delete($old_path);

        $facility->delete();

        return redirect()->route('facility.index')->with('success', 'Success Delete Fasilitas');
    }
}
