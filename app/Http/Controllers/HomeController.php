<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Facility;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $employee = User::count();
        $customer = Customer::count();
        $room = Room::where('status','available')
        ->count();
        $facility = Facility::count();

        $data = array(
            'employee' => $employee,
            'customer' => $customer,
            'room' => $room,
            'facility' => $facility,
        );

        return view('home', $data);

    }
}
