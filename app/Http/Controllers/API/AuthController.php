<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        try {
            $validateData = $request->validate([
                'name' => 'required',
                'email' => 'required|email|unique:customer',
                'phone_number' => 'required',
                'password' => 'required',
            ]);

            $data = $request->all();
            $data['password'] = bcrypt($data['password']);

            $customer = Customer::create([
                'name' => $request->name,
                'email' => $request->email,
                'phone_number' => $request->phone_number,
                'password' => $data['password']
            ]);

            $tokenResult = $customer->createToken('authToken')->plainTextToken;

            if($customer) {
                return ResponseFormatter::success([
                    'access_token' => $tokenResult,
                    'token_type' => 'Bearer',
                    'user' => $customer
                ],
            'Customer create successfully'
                );

            }
            else {
                return ResponseFormatter::error(
                    null,
                    'Customer failed to create',
                    404,
                );
            }
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error->validator->errors(),
            ], 'Authentication Failed', 500);
        }
    }

    public function login(Request $request)
    {
        try {
            $this->validate($request,[
                'email' => 'required|email',
                'password' => 'required',
            ]);

            $credentials = request(['email','password']);

            if (Auth::guard('customer')->attempt($credentials)) {
                $customer = Customer::where('email', $request->email)->first();

                if (! Hash::check($request->password, $customer->password, [])) {
                    throw new \Exception('Invalid Credentials');
                }

                $tokenResult = $customer->createToken('authToken')->plainTextToken;

                return ResponseFormatter::success([
                    'access_token' => $tokenResult,
                    'token_type' => 'Bearer',
                    'user' => $customer,
                ], 'Authenticated');
            } else {
                return ResponseFormatter::error([
                    'message' => 'Unauthorized',
                ], 'Authentication Failed', 500);
            }
        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error->validator->errors(),
            ], 'Authentication Failed', 500);
        }
    }

    public function getCustomer()
    {
        $customer = auth()->user();
        return $customer;
    }

    public function getCustomerOrder()
    {
        $customer = auth()->user();

        $order = Order::with(['orderDetail.room','customer'])
            ->where('customer_id',$customer->id)
            ->get();

            return ResponseFormatter::success(
                $order,
                'Order data retrieved successfully'
            );
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return ResponseFormatter::success(
            'Logout Success',
        );
    }

}
