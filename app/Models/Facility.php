<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Facility extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'facility';

    protected $fillable = [
        'room_category_id','name','description','url_image',
    ];

    public function roomCategory()
    {
        return $this->belongsTo(CategoryRoom::class, 'room_category_id', 'id');
    }
}
