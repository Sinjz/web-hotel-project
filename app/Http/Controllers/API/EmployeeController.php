<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function indexEmployee()
    {
        $employee = User::all();

        if($employee)
            return ResponseFormatter::success(
                $employee,
                'Employee data retrieved successfully'
            );
        else
            return ResponseFormatter::error(
                null,
                'Employee data not exist',
                404,
            );
    }
}
